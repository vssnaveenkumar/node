var express=require('express');
var mysqlapi=require('./mysqlapi.js');
var app=express();
var bodyParser = require('body-parser');
var cors=require('cors');
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/',function(req,res){
	mysqlapi.getAll(function(err,rows){
		if(err)
			res.json(err);
		else
			res.json(rows);

	});
});
app.get('/log',function(req,res){
	mysqlapi.getAllLog(req.query,function(err,rows){
		if(err)
			res.json(err);
		else
			res.json(rows);

	});
});
app.get('/viewlog',function(req,res){
	MongoClient.connect(url, function(err, db) {
		var dbo = db.db("test");
		dbo.collection("log").find({}).toArray(function(err, result) {
			if (err) throw err;
			res.json(result);
			db.close();
		});
	});
});
app.post('/insert',function(req,res){
	mysqlapi.insertData(req.query,function(err,rows){
		if(err)
			res.json(err);
		else
			res.json(rows);

	});
});
app.post('/update',function(req,res){

});
app.post('/delete',function(req,res){

});
app.listen(3000);